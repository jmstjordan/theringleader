<?php
session_start();

// Pear Mail Library
try{
	require_once "/usr/share/php/Mail.php";
}catch (Exception $e){
	header('Location: index.php');
	die();
}
$from = '<janmichaeljordan@gmail.com>';
$to = '<njordan@eltech.net>, <jmstjordan@gmail.com>';
$subject = 'Inquiry form';
$body =  "Name: " . $_POST['userName'] . "\nPhone Number: " . $_POST['userPhone'] . "\nEmail Address: " . $_POST['userEmail'] . "\nCity: " . $_POST['userCity'] . "\nDescription: " . $_POST['userDescription'];
$headers = array(
    'From' => $from,
    'To' => $to,
    'Subject' => $subject
);

$smtp = Mail::factory('smtp', array(
        'host' => 'ssl://smtp.gmail.com',
        'port' => '465',
        'auth' => true,
        'username' => 'janmichaeljordan@gmail.com',
        'password' => ''
    ));

$mail = $smtp->send($to, $headers, $body);

if (PEAR::isError($mail)) {
    echo('<p>' . $mail->getMessage() . '</p>');
} else {
    echo('<p>Message successfully sent!</p>');
    $_SESSION['success.mail'] = true;
    header('Location: index.php');
    die();
}

?>
