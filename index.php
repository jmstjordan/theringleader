<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>The Ring Leader | Property Inspectors</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/portfolio-item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Norm Jordan - (208) 340-0585</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>-->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">The Ring Leader
                    <small>Property Inspectors</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-8">
                <img class="img-responsive" src="css/white_banner.png" alt="">
            </div>

            <div class="col-md-4">
                <h3>What sets THE RING LEADER apart from the other property inspectors? The answer is 5 words:</h3>
                <ol>
                    <li>Experience</li>
                    <li>Education</li>
                    <li>Certifications</li>
                    <li>Integrity</li>
		    <li>Fair Pricing</li>
                </ol>
		<p>We have 25 plus years in new home construction and remodeling. Academically, we are prepared on the Master’s Degree level.</p><p>
			We are board certified by the American Society of Clinical Pathologists in the areas of Clinical Chemistry (Radon and Radon Decay Products), Mycology
			(Fungi and Mold). We are also certified by INTERNACHI in the areas such as water quality, moisture intrusion and meth house hazards.
		</p>
		<h3>Contact Information</h3>
		<ul>
		    <li>Norm Jordan</li>
		    <li>Boise, ID</li>
		    <li>(208) 340-0585</li>
                    <li>njordan@eltech.net</li>
		</ul>
            </div>

        </div>
        <!-- /.row -->

        <!-- Related Projects Row -->
        <div class="row">

            <div class="col-lg-12">
                <h3 class="page-header">Related Projects</h3>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="css/vet_owned.png" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="css/iac.png" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="css/wqt.png" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="css/internachi.png" alt="">
                </a>
            </div>

        </div>
        <!-- /.row -->

        <hr>
<form method="post" action="mail_trl.php">
  <fieldset class="form-group">
    <label for="formGroupExampleInput">Name</label>
    <input type="text" class="form-control" id="Name" name="userName" maxlength="100" placeholder="What is your name?">
  </fieldset>
  <fieldset class="form-group">
    <label for="formGroupExampleInput2">Phone Number</label>
    <input type="text" class="form-control bfh-phone" name="userPhone" maxlength="100" placeholder="What is your phone number?" data-format="+1 (ddd) ddd-dddd">
  </fieldset>
  <fieldset class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="userEmail" maxlength="100" placeholder="What is your email?">
    <small class="text-muted">We'll never share your email with anyone else.</small>
  </fieldset>
  <fieldset class="form-group">
    <label for="exampleSelect1">City</label>
    <select class="form-control" id="cityChoice" name="userCity">
      <option>Boise</option>
      <option>Meridean</option>
      <option>Eagle</option>
      <option>Nampa</option>
      <option>Other</option>
    </select>
  </fieldset>
  <fieldset class="form-group">
    <label for="exampleTextarea">Describe what kind of service you need</label>
    <textarea class="form-control" name="userDescription" id="exampleTextarea" maxlength="1000" rows="3"></textarea>
  </fieldset>
    <button type="submit" class="btn btn-primary">Submit</button>
    <?php if(isset($_SESSION['success.mail'])){ session_unset($SESSION['success.mail']); ?>
        <h5 style="color: green">Success! You will be contacted shortly</h5>
    <?php } ?>
</form>
<div id="form_success"></div>
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TheRingLeader.Net 2017</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
